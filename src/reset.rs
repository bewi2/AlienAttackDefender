use crate::{
    Attack, AttackSize, AttackSpeed, Enemy, EnemySize, EnemySpeedRange, Game, GameOver,
    HealthBoard, LevelBoard, ScoreBoard, Speed, Splat, Tower,
};
use bevy::{core::FixedTimestep, prelude::*};

struct RestartEvent;

pub struct ResetPlugin;

impl Plugin for ResetPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::new()
                .with_run_criteria(
                    FixedTimestep::step(1.0 / 120.), // lock sytems to 120 refresh rate
                )
                .with_system(restart)
                .with_system(reset_values)
                .with_system(reset_text)
                .with_system(reset_entities),
        )
        .add_event::<RestartEvent>();
    }
}

fn restart(
    mut game: ResMut<Game>,
    keyboard_input: Res<Input<KeyCode>>,
    mut restart_event: EventWriter<RestartEvent>,
) {
    if !game.tower_state && keyboard_input.pressed(KeyCode::Return) {
        restart_event.send(RestartEvent);
        game.tower_state = true;
    }
}

fn reset_entities(
    mut commands: Commands,
    mut tower_query: Query<(&mut Transform, &mut Speed), With<Tower>>,
    mut enemy_query: Query<Entity, With<Enemy>>,
    mut attack_query: Query<(Entity, &mut AttackSpeed), With<Attack>>,
    mut text_query: Query<Entity, With<GameOver>>,
    mut restart_event: EventReader<RestartEvent>,
) {
    for _event in restart_event.iter() {
        for (mut tower_tf, mut tower_speed) in tower_query.iter_mut() {
            tower_tf.rotation = Quat::from_rotation_z(0.);
            tower_speed.0 = 3.;
        }

        for enemy_entity in enemy_query.iter_mut() {
            commands.entity(enemy_entity).despawn();
        }

        for (attack_entity, mut attack_speed) in attack_query.iter_mut() {
            commands.entity(attack_entity).despawn();
            attack_speed.0 = 2.;
        }

        for text_entity in text_query.iter_mut() {
            commands.entity(text_entity).despawn();
        }
    }
}

fn reset_values(
    mut commands: Commands,
    mut game: ResMut<Game>,
    mut enemy_speed_range: ResMut<EnemySpeedRange>,
    mut attack_size: ResMut<AttackSize>,
    mut enemy_size: ResMut<EnemySize>,
    mut restart_event: EventReader<RestartEvent>,
    splat_query: Query<Entity, With<Splat>>,
) {
    for _event in restart_event.iter() {
        for splat_entity in splat_query.iter() {
            commands.entity(splat_entity).despawn();
        }

        game.max_enemies = 10;
        game.num_enemies = 0;
        enemy_speed_range.low = 1.5;
        enemy_speed_range.high = 2.5;
        enemy_size.x = 0.5;
        enemy_size.y = 0.5;
        attack_size.x = 3.7;
        attack_size.y = 3.7;
    }
}

// This is the implementation recommended by the Bevy cheatbook and has been implemented
// in Bevy for this exact use case, so the clippy error is expected here.
fn reset_text(
    mut game: ResMut<Game>,
    mut query_set: QuerySet<(
        QueryState<&mut Text, With<HealthBoard>>,
        QueryState<&mut Text, With<ScoreBoard>>,
        QueryState<&mut Text, With<LevelBoard>>,
    )>,
    mut restart_event: EventReader<RestartEvent>,
) {
    for _event in restart_event.iter() {
        game.tower_health = 10;
        game.score = 0;
        game.current_level = 1;

        for mut health_text in query_set.q0().iter_mut() {
            health_text.sections[0].value = format!("Health: {}", game.tower_health);
        }
        for mut score_text in query_set.q1().iter_mut() {
            score_text.sections[0].value = format!("Score: {}", game.score);
        }
        for mut level_text in query_set.q2().iter_mut() {
            level_text.sections[0].value = format!("Level: {}", game.current_level);
        }
    }
}
