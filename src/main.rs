use bevy::{core::FixedTimestep, prelude::*, sprite::collide_aabb::collide};
use enemy::EnemyPlugin;
use reset::ResetPlugin;
use tower::TowerPlugin;

mod enemy;
mod reset;
mod tower;

const TIME_STEP: f32 = 1. / 60.;

#[derive(Debug, Hash, PartialEq, Eq, Clone, StageLabel)]
struct FixedUpdateStage;

struct WinSize {
    #[allow(unused)]
    w: f32,
    h: f32,
}

#[derive(Component)]
struct Game {
    tower_state: bool,
    tower_health: i32,
    num_enemies: u32,
    score: u32,
    max_enemies: u32,
    current_level: u32,
}

#[derive(Component)]
struct Tower;

#[derive(Component)]
struct Speed(f32);

#[derive(Component)]
struct Attack;

#[derive(Component)]
struct AttackSpeed(f32);

#[derive(Component)]
struct AttackSize {
    x: f32,
    y: f32,
    size: Vec2,
}

#[derive(Component)]
struct CreateSplat(Vec3);

#[derive(Component)]
struct Enemy;

#[derive(Component)]
struct EnemySize {
    x: f32,
    y: f32,
    size: Vec2,
}

#[derive(Component)]
struct EnemySpeed(f32);

#[derive(Component)]
struct EnemySpeedRange {
    low: f32,
    high: f32,
}

#[derive(Component)]
struct ScoreBoard;

#[derive(Component)]
struct HealthBoard;

#[derive(Component)]
struct LevelBoard;

#[derive(Component)]
struct GameOver;

#[derive(Component)]
struct Splat;

#[derive(Component)]
struct BackGround;

struct GameoverEvent;
struct EnemyHitEvent(Vec3, Vec3);

fn main() {
    App::new()
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(
                    FixedTimestep::step(1.0 / 120.), // lock sytems to 120 refresh rate
                )
                .with_system(tower_hit_enemy)
                .with_system(enemy_hit_tower)
                .with_system(end_game)
                .with_system(update_window)
                .with_system(increase_difficulty),
        )
        .add_system(bevy::input::system::exit_on_esc_system)
        .insert_resource(WindowDescriptor {
            title: "Tower Defense".to_string(),
            mode: bevy::window::WindowMode::BorderlessFullscreen,
            ..Default::default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(TowerPlugin)
        .add_plugin(EnemyPlugin)
        .add_plugin(ResetPlugin)
        .add_startup_system(setup)
        .add_event::<GameoverEvent>()
        .add_event::<EnemyHitEvent>()
        .run();
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>, mut windows: ResMut<Windows>) {
    let window = windows.get_primary_mut().unwrap();

    commands.spawn_bundle(OrthographicCameraBundle::new_2d());

    commands.insert_resource(WinSize {
        w: window.width(),
        h: window.height(),
    });

    commands
        .spawn_bundle(SpriteBundle {
            transform: Transform {
                rotation: Quat::from_rotation_z(0.),
                scale: Vec3::new(window.width() / 600., window.height() / 600., 1.),
                ..Default::default()
            },
            texture: asset_server.load("SpaceBackground.png"),
            ..Default::default()
        })
        .insert(BackGround);

    let font = asset_server.load("FiraSans-Bold.ttf");
    let text_style = TextStyle {
        font,
        font_size: 60.0,
        color: Color::WHITE,
    };

    let text_alignment = TextAlignment {
        vertical: VerticalAlign::Center,
        horizontal: HorizontalAlign::Center,
    };

    commands
        .spawn_bundle(Text2dBundle {
            text: Text::with_section("Health: 10".to_string(), text_style.clone(), text_alignment),
            transform: Transform {
                translation: Vec3::new(
                    (window.width() / 2.) - 150.,
                    (window.height() / 2.) - 50.,
                    40.,
                ),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(HealthBoard);

    commands
        .spawn_bundle(Text2dBundle {
            text: Text::with_section("Score: 0".to_string(), text_style, text_alignment),
            transform: Transform {
                translation: Vec3::new(
                    (-window.width() / 2.) + 150.,
                    (window.height() / 2.) - 50.,
                    40.,
                ),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(ScoreBoard);

    commands
        .spawn_bundle(Text2dBundle {
            text: Text::with_section(
                "Level: 1".to_string(),
                TextStyle {
                    font: asset_server.load("FiraSans-Bold.ttf"),
                    font_size: 80.0,
                    color: Color::WHITE,
                },
                text_alignment,
            ),
            transform: Transform {
                translation: Vec3::new(0., (window.height() / 2.) - 50., 20.),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(LevelBoard);

    commands.insert_resource(Game {
        tower_state: true,
        tower_health: 10,
        num_enemies: 0,
        score: 0,
        max_enemies: 10,
        current_level: 1,
    });

    commands.insert_resource(EnemySpeedRange {
        low: 1.5,
        high: 2.5,
    });

    // Some wizardery from Jeremy Chone here https://github.com/jeremychone-channel/rust-invaders/blob/main/src/player.rs
    let path = std::path::Path::new("assets").join("IbanBlast.png");
    let bytes = std::fs::read(&path).unwrap_or_else(|_| panic!("Cannot find {}", path.display()));
    let image = Image::from_buffer(
        &bytes,
        bevy::render::texture::ImageType::MimeType("image/png"),
    )
    .unwrap();
    let size = image.texture_descriptor.size;
    let mut attack_vec = Vec2::new(size.width as f32, size.height as f32);
    attack_vec *= 2. / 3.;

    commands.insert_resource(AttackSize {
        x: 3.7,
        y: 3.7,
        size: attack_vec,
    });

    let path = std::path::Path::new("assets").join("2d_alien.png");
    let bytes = std::fs::read(&path).unwrap_or_else(|_| panic!("Cannot find {}", path.display()));
    let image = Image::from_buffer(
        &bytes,
        bevy::render::texture::ImageType::MimeType("image/png"),
    )
    .unwrap();
    let size = image.texture_descriptor.size;
    let mut enemy_vec = Vec2::new(size.width as f32, size.height as f32);
    enemy_vec *= 2. / 3.;

    commands.insert_resource(EnemySize {
        x: 0.5,
        y: 0.5,
        size: enemy_vec,
    });
}

// This is the implementation recommended by the Bevy cheatbook and has been implemented
// in Bevy for this exact use case, so the clippy error is expected here.
fn update_window(
    mut windows: ResMut<Windows>,
    mut win_size: ResMut<WinSize>,
    mut query_set: QuerySet<(
        QueryState<&mut Transform, With<BackGround>>,
        QueryState<&mut Transform, With<HealthBoard>>,
        QueryState<&mut Transform, With<ScoreBoard>>,
        QueryState<&mut Transform, With<LevelBoard>>,
    )>,
) {
    let window = windows.get_primary_mut().unwrap();

    if window.width() != win_size.w || window.height() != win_size.h {
        win_size.w = window.width();
        win_size.h = window.height();

        for mut background_tf in query_set.q0().iter_mut() {
            background_tf.scale.x = win_size.w / 600.;
            background_tf.scale.y = win_size.h / 600.;
        }

        for mut health_tf in query_set.q1().iter_mut() {
            health_tf.translation.x = (win_size.w / 2.) - 150.;
            health_tf.translation.y = (win_size.h / 2.) - 50.;
        }
        for mut score_tf in query_set.q2().iter_mut() {
            score_tf.translation.x = (-win_size.w / 2.) + 150.;
            score_tf.translation.y = (win_size.h / 2.) - 50.;
        }
        for mut level_tf in query_set.q3().iter_mut() {
            level_tf.translation.y = (win_size.h / 2.) - 50.;
        }
    }
}

fn tower_hit_enemy(
    mut commands: Commands,
    mut enemy_hit_event: EventWriter<EnemyHitEvent>,
    attack_size: ResMut<AttackSize>,
    enemy_size: Res<EnemySize>,
    attack_query: Query<(Entity, &Transform), With<Attack>>,
    enemy_query: Query<(Entity, &Transform), With<Enemy>>,
) {
    for (attack_entity, attack_tf) in attack_query.iter() {
        let attack_scale = Vec2::new(attack_tf.scale.x, attack_tf.scale.y);

        for (enemy_entity, enemy_tf) in enemy_query.iter() {
            let enemy_scale = Vec2::new(enemy_tf.scale.x, enemy_tf.scale.y);

            let collision = collide(
                attack_tf.translation,
                attack_scale * attack_size.size,
                enemy_tf.translation,
                enemy_scale * enemy_size.size,
            );

            if collision.is_some() {
                enemy_hit_event.send(EnemyHitEvent(enemy_tf.translation, enemy_tf.scale));

                commands.entity(attack_entity).despawn();
                commands.entity(enemy_entity).despawn();
            }
        }
    }
}

fn enemy_hit_tower(
    mut commands: Commands,
    mut game: ResMut<Game>,
    enemy_size: Res<EnemySize>,
    tower_query: Query<&Transform, With<Tower>>,
    enemy_query: Query<(Entity, &Transform), With<Enemy>>,
    mut health_query: Query<&mut Text, With<HealthBoard>>,
    mut gameover_event: EventWriter<GameoverEvent>,
) {
    for tower_tf in tower_query.iter() {
        let tower_scale = Vec2::new(tower_tf.scale.x, tower_tf.scale.y);

        for (enemy_entity, enemy_tf) in enemy_query.iter() {
            let enemy_scale = Vec2::new(enemy_tf.scale.x, enemy_tf.scale.y);

            let collision = collide(
                tower_tf.translation,
                tower_scale * 200.,
                enemy_tf.translation,
                enemy_scale * enemy_size.size,
            );

            if collision.is_some() {
                commands.entity(enemy_entity).despawn();

                game.tower_health -= 1;

                if game.tower_health <= 0 && game.tower_state {
                    game.tower_state = false;
                    gameover_event.send(GameoverEvent);
                } else {
                    game.num_enemies -= 1;
                }

                let mut text = health_query.single_mut();
                text.sections[0].value = format!("Health: {}", game.tower_health);
            }
        }
    }
}

fn increase_difficulty(
    mut game: ResMut<Game>,
    mut enemy_size: ResMut<EnemySize>,
    mut attack_size: ResMut<AttackSize>,
    mut enemy_speed_range: ResMut<EnemySpeedRange>,
    mut level_query: Query<&mut Text, With<LevelBoard>>,
) {
    if game.score == 50 && game.current_level != 2 {
        game.current_level += 1;
        game.max_enemies += 5;

        enemy_speed_range.high += 0.5;

        let mut text = level_query.single_mut();
        text.sections[0].value = format!("Level: {}", game.current_level);
    }

    if game.score == 120 && game.current_level != 3 {
        game.current_level += 1;

        game.max_enemies += 5;

        enemy_size.x /= 3. / 2.;
        enemy_size.y /= 3. / 2.;

        attack_size.x *= 3. / 4.;
        attack_size.y *= 3. / 4.;

        enemy_speed_range.low += 0.5;
        enemy_speed_range.high += 0.5;

        let mut text = level_query.single_mut();
        text.sections[0].value = format!("Level: {}", game.current_level);
    }

    if game.score == 270 && game.current_level != 4 {
        game.current_level += 1;
        game.max_enemies += 5;

        enemy_size.x /= 3. / 2.;
        enemy_size.y /= 3. / 2.;

        let mut text = level_query.single_mut();
        text.sections[0].value = format!("Level: {}", game.current_level);
    }

    if game.score >= 460 && game.current_level != 5 {
        game.current_level += 1;
        game.max_enemies += 5;

        enemy_speed_range.low += 0.5;
        enemy_speed_range.high += 0.5;

        let mut text = level_query.single_mut();
        text.sections[0].value = "Level: Good Luck".to_string();
    }
}

fn end_game(
    mut commands: Commands,
    asset_server: ResMut<AssetServer>,
    mut game: ResMut<Game>,
    mut tower_query: Query<&mut Speed, With<Tower>>,
    mut enemy_query: Query<&mut EnemySpeed, With<Enemy>>,
    mut attack_query: Query<&mut AttackSpeed, With<Attack>>,
    mut gameover_event: EventReader<GameoverEvent>,
) {
    for _event in gameover_event.iter() {
        game.max_enemies = 0;
        for mut tower_speed in tower_query.iter_mut() {
            tower_speed.0 = 0.;
        }

        for mut enemy_speed in enemy_query.iter_mut() {
            enemy_speed.0 = 0.;
        }

        for mut attack_speed in attack_query.iter_mut() {
            attack_speed.0 = 0.;
        }

        game.tower_state = false;

        let font = asset_server.load("FiraSans-Bold.ttf");
        let text_style = TextStyle {
            font,
            font_size: 60.0,
            color: Color::WHITE,
        };

        let text_alignment = TextAlignment {
            vertical: VerticalAlign::Center,
            horizontal: HorizontalAlign::Center,
        };

        commands
            .spawn_bundle(Text2dBundle {
                text: Text::with_section("GAME OVER".to_string(), text_style, text_alignment),
                transform: Transform {
                    translation: Vec3::new(0., 100., 50.),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(GameOver);

        commands
            .spawn_bundle(Text2dBundle {
                text: Text::with_section(
                    "Press Enter to play again".to_string(),
                    TextStyle {
                        font: asset_server.load("FiraSans-Bold.ttf"),
                        font_size: 40.0,
                        color: Color::WHITE,
                    },
                    text_alignment,
                ),
                transform: Transform {
                    translation: Vec3::new(0., -100., 50.),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(GameOver);
    }
}
