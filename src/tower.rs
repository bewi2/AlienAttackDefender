use crate::{Attack, AttackSize, AttackSpeed, Game, Speed, Tower, WinSize, TIME_STEP};
use bevy::{core::FixedTimestep, prelude::*};

pub struct TowerPlugin;

impl Plugin for TowerPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::new()
                .with_run_criteria(
                    FixedTimestep::step(1.0 / 120.), // lock sytems to 120 refresh rate
                )
                .with_system(tower_movement)
                .with_system(attack_movement),
        )
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(0.2))
                .with_system(tower_fire),
        )
        .add_startup_system(spawn_tower.system());
    }
}

fn spawn_tower(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands
        .spawn_bundle(SpriteBundle {
            transform: Transform {
                translation: Vec3::new(1., 1., 20.),
                rotation: Quat::from_rotation_z(0.),
                scale: Vec3::new(0.2, 0.2, 1.),
            },
            texture: asset_server.load("SpaceShip.png"),
            ..Default::default()
        })
        .insert(Tower)
        .insert(Speed(3.));
}

fn tower_movement(
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<(&Speed, &mut Transform), With<Tower>>,
) {
    if let Ok((speed, mut tower_tf)) = query.get_single_mut() {
        if keyboard_input.pressed(KeyCode::Right) {
            let mut temp = tower_tf.rotation;
            temp *= Quat::from_rotation_z(-1. * speed.0 * TIME_STEP * (2. / 3.));

            if temp.to_axis_angle().1 < tower_tf.rotation.to_axis_angle().1 {
                tower_tf.rotation = Quat::from_rotation_z(0.);
            }

            tower_tf.rotation *= Quat::from_rotation_z(-1. * speed.0 * TIME_STEP * (2. / 3.));
        } else if keyboard_input.pressed(KeyCode::Left) {
            let mut temp = tower_tf.rotation;
            temp *= Quat::from_rotation_z(1. * speed.0 * TIME_STEP * (2. / 3.));

            if temp.to_axis_angle().1 > tower_tf.rotation.to_axis_angle().1 {
                tower_tf.rotation = Quat::from_rotation_z(2. * std::f32::consts::PI);
            }

            tower_tf.rotation *= Quat::from_rotation_z(1. * speed.0 * TIME_STEP * (2. / 3.));
        }
    }
}

fn tower_fire(
    mut commands: Commands,
    keyboard_input: Res<Input<KeyCode>>,
    asset_server: Res<AssetServer>,
    attack_size: ResMut<AttackSize>,
    game: Res<Game>,
    mut query: Query<&Transform, With<Tower>>,
) {
    if let Ok(tower_tf) = query.get_single_mut() {
        if keyboard_input.pressed(KeyCode::Space) && game.tower_state {
            let tower_rotation = tower_tf.rotation;

            commands
                .spawn_bundle(SpriteBundle {
                    transform: Transform {
                        translation: Vec3::new(0., 0., 10.),
                        rotation: tower_rotation,
                        scale: Vec3::new(attack_size.x, attack_size.y, 10.),
                    },
                    texture: asset_server.load("IbanBlast.png"),
                    ..Default::default()
                })
                .insert(AttackSpeed(2.))
                .insert(Attack);
        }
    }
}

fn attack_movement(
    mut commands: Commands,
    win_size: Res<WinSize>,
    mut query: Query<(Entity, &AttackSpeed, &mut Transform), With<Attack>>,
) {
    for (attack_entity, speed, mut attack_tf) in query.iter_mut() {
        let axis = attack_tf.rotation.to_axis_angle();

        attack_tf.translation.x += speed.0 * axis.1.sin() * TIME_STEP * 60.;
        attack_tf.translation.y += speed.0 * axis.1.cos() * TIME_STEP * 60.;

        if attack_tf.translation.x > win_size.w
            || attack_tf.translation.y > win_size.h
            || attack_tf.translation.x < -win_size.w
            || attack_tf.translation.y < -win_size.h
        {
            commands.entity(attack_entity).despawn();
        }
    }
}
