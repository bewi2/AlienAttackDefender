use crate::{
    Enemy, EnemyHitEvent, EnemySize, EnemySpeed, EnemySpeedRange, Game, ScoreBoard, Splat, WinSize,
    TIME_STEP,
};
use bevy::{core::FixedTimestep, prelude::*};
use rand::{thread_rng, Rng};

pub struct EnemyPlugin;

impl Plugin for EnemyPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::new()
                .with_run_criteria(
                    FixedTimestep::step(1.0 / 120.), // lock sytems to 120 refresh rate
                )
                .with_system(spawn_enemy)
                .with_system(enemy_movement)
                .with_system(fade_splat)
                .with_system(spawn_splat),
        );
    }
}

fn spawn_enemy(
    mut commands: Commands,
    win_size: Res<WinSize>,
    asset_server: Res<AssetServer>,
    mut game: ResMut<Game>,
    enemy_size: ResMut<EnemySize>,
    speed_range: ResMut<EnemySpeedRange>,
) {
    if game.num_enemies < game.max_enemies && game.tower_state {
        let min_w_distance = win_size.w / 2.;
        let min_h_distance = win_size.h / 2.;

        let x_coord: f32;
        let y_coord: f32;

        let rand: u32 = thread_rng().gen_range(0..6);
        if rand == 0 {
            x_coord = thread_rng().gen_range(min_w_distance + 20.0..(min_w_distance + 80.));
            y_coord = thread_rng().gen_range(-min_h_distance..min_h_distance);
        } else if rand == 1 {
            y_coord = thread_rng().gen_range(min_h_distance + 20.0..(min_h_distance + 80.));
            x_coord = thread_rng().gen_range(-min_w_distance..-200.0);
        } else if rand == 2 {
            y_coord = thread_rng().gen_range(min_h_distance + 20.0..(min_h_distance + 80.));
            x_coord = thread_rng().gen_range(200.0..min_w_distance);
        } else if rand == 3 {
            x_coord = thread_rng().gen_range((-min_w_distance - 80.)..-min_w_distance - 20.);
            y_coord = thread_rng().gen_range(-min_h_distance..min_h_distance);
        } else if rand == 4 {
            y_coord = thread_rng().gen_range((-min_h_distance - 80.)..-min_h_distance - 20.);
            x_coord = thread_rng().gen_range(-min_w_distance..-200.0);
        } else {
            y_coord = thread_rng().gen_range((-min_h_distance - 80.)..-min_h_distance - 20.);
            x_coord = thread_rng().gen_range(200.0..min_w_distance);
        }
        let speed = thread_rng().gen_range(speed_range.low..speed_range.high);

        commands
            .spawn_bundle(SpriteBundle {
                transform: Transform {
                    translation: Vec3::new(x_coord, y_coord, 10.),
                    scale: Vec3::new(enemy_size.x, enemy_size.y, 10.),
                    ..Default::default()
                },
                texture: asset_server.load("2d_alien.png"),
                ..Default::default()
            })
            .insert(EnemySpeed(speed))
            .insert(Enemy);

        game.num_enemies += 1;
    }
}

fn enemy_movement(mut query: Query<(&mut EnemySpeed, &mut Transform), With<Enemy>>) {
    for (speed, mut enemy_tf) in query.iter_mut() {
        let slope = enemy_tf.translation.y / enemy_tf.translation.x;
        let mut x = enemy_tf.translation.x;

        if x < 0. {
            x += speed.0 * TIME_STEP * 10.;
        } else {
            x -= speed.0 * TIME_STEP * 10.;
        }
        let y = slope * x;

        enemy_tf.translation.x = x;
        enemy_tf.translation.y = y;
    }
}

fn spawn_splat(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut game: ResMut<Game>,
    mut enemy_hit_event: EventReader<EnemyHitEvent>,
    mut score_query: Query<&mut Text, With<ScoreBoard>>,
) {
    for event in enemy_hit_event.iter() {
        game.score += 1;
        game.num_enemies -= 1;

        let mut text = score_query.single_mut();
        text.sections[0].value = format!("Score: {}", game.score);

        commands
            .spawn_bundle(SpriteBundle {
                sprite: Sprite {
                    color: Color::rgba(1., 1., 1., 1.0),
                    // color: Color::RED,
                    ..Default::default()
                },
                texture: asset_server.load("green_splat.png"),
                transform: Transform {
                    translation: Vec3::new(event.0[0], event.0[1], 5.),
                    scale: Vec3::new(event.1[0] / 3., event.1[1] / 3., 10.),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Splat)
            .insert(Timer::from_seconds(2.0, false));
    }
}

fn fade_splat(
    mut commands: Commands,
    time: Res<Time>,
    game: Res<Game>,
    mut splat_query: Query<(Entity, &mut Sprite, &mut Timer), With<Splat>>,
) {
    for (splat_entity, mut splat_sprite, mut timer) in splat_query.iter_mut() {
        if game.tower_state {
            timer.tick(time.delta());
            if timer.finished() {
                let sprite_alpha = splat_sprite.color.a() * 0.98;
                splat_sprite.color.set_a(sprite_alpha);
            }

            if splat_sprite.color.a() < 0.01 {
                commands.entity(splat_entity).despawn();
            }
        }
    }
}
