# Alien Attack Defender

You are stranded in space defending against an increasing onslaught of alien attackers.

Defend your ship by rotating with the left and right arrow keys and attacking by holding the spacebar. 

Can you make it to the final wave?

# Setup 

For Windows 10:
- Ensure that Rust and Cargo are installed. 
- Clone this repository to your local machine.
- Go to the location of the AlienAttackDefender folder and start the game using cargo run. 
- Likely, several packages will need to compile so it may take some time. If compilation fails for any package, remove the cargo.lock file and try running again. 

# About

Game made with the Rust programming language using the Bevy Game Engine v0.6.0. 

# Notes on the Project for CS410 Grading

This game turned into a slight derivative of the tower defense game that I had originally set forth in the project proposal, as it goes. However, I am very happy to be able to deliver this as my project as I think it turned out really well. This game works by using the Bevy game engine which is one of the more advanced engine frameworks available that is written Rust. I had never made a game before so the learning curve for this was quite intense. On top of this, a new realease of the engine just six months ago made many third party examples and code snippets outdated. There isn't really a way to do any testing in this case and because the framework is already in place, there is about 800 lines of code. Regardless I learned a lot during the development of this project of not only Bevy but also Rust. 

# Credits

- [Bevy Engine](https://bevyengine.org/)
- [Bevy cheatbook](https://bevy-cheatbook.github.io/)
- A Bevy tutorial guide by Jeremy Chone - [Rust Invaders](https://github.com/jeremychone-channel/rust-invaders)
